extern crate libc;

use libc::c_int;
use libc::c_char;
// C API calls
extern "C" {
    /// Get LZ4 lib version number
    fn LZ4_versionNumber() -> c_int;
    
    /// Get the number of bytes required in the output buffer
    /// to successfully compress a data block of the given
    /// size
    fn LZ4_compressBound(size: c_int) -> c_int;
  
    /// Compress a block o data
    /// Returns 0 on failure, size of compressed data otherwise
    fn LZ4_compress_HC(input:  *const c_char, 
                       output: *mut c_char,
                       input_size: c_int,
                       output_size: c_int,
                       compression_level: c_int) -> c_int;

    fn LZ4_decompress_safe(input: *const c_char,
                           output: *mut c_char,
                           input_size: c_int,
                           output_size: c_int) -> c_int;
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Version {
    pub major: i32,
    pub minor: i32,
    pub release: i32,
}

pub fn get_version() -> Version {
    let version = unsafe { LZ4_versionNumber() as i32 };

    Version {
        major: version / (100 * 100),
        minor: (version / 100) % 100,
        release: version % 100,
    }
}

pub enum CompressionErrorReason {
    OutputSizeInvalid,
    OutputSizeInsufficient,
    InputInvalid,
    CompressionFailure
}

pub type CompressionResult = Result<usize, CompressionErrorReason>;
pub type DecompressionResult = Result<usize,()>;

/// Check whether a given input size can be compressed into a buffer
/// of a given output size.
pub fn can_compress(size_input:usize, size_output:usize) -> bool {
    if size_input >= i32::max_value() as usize {
        return false;
    }
    let required_size = unsafe {
        LZ4_compressBound(size_input as i32) as usize
    };
    // We can only compress this block of our storage
    // is <= required_size 
    required_size > 0 && required_size <= size_output
}

/// Compress a block of data from input into output using LZ4 HC 
/// compression. 
pub fn compress_hc(output:&mut [u8], input:&[u8]) -> CompressionResult {
    let input_size = input.len();
    if input_size == 0 {
        return Err(CompressionErrorReason::InputInvalid);
    }
    
    let result;
    unsafe {
        let ptr_input = input.as_ptr();
        let ptr_output = output.as_mut_ptr();
        result = LZ4_compress_HC(ptr_input as *const c_char,
                                 ptr_output as *mut c_char,
                                 input.len() as c_int,
                                 output.len() as c_int,
                                 9);
    }

    if result == 0 {
        if !can_compress(input_size, output.len()) {
            return Err(CompressionErrorReason::OutputSizeInsufficient);
        }
        return Err(CompressionErrorReason::CompressionFailure);
    }
    Ok(result as usize)
}

/// Decompress a block of compressed data from input into output.
/// Note: The slice must be the exact size of the compressed data for 
/// this function to work correctly.
pub fn decompress(output:&mut[u8], input:&[u8]) -> DecompressionResult {
    let input_size = input.len();
    let output_size = output.len();
    let result;
    unsafe {
        let ptr_input = input.as_ptr();
        let ptr_output = output.as_ptr(); 
        result = LZ4_decompress_safe(ptr_input as *const c_char,
                                     ptr_output as *mut c_char,
                                     input_size as c_int,
                                     output_size as c_int);
    }

    if result < 0 {
        return Err(());
    }

    Ok(result as usize)
}

/// Maintains the state for the XXHash 64 bit calculations.
/// Be sure to initialize it with the reset() function after 
/// creation.
#[repr(C)]
pub struct XXH64_State {
    total_len: u64,
    v1: u64,
    v2: u64,
    v3: u64,
    v4: u64,
    mem64:[u64;4],
    memsize:u32,
    reserved:[u32;2]
}

use libc::{c_void, size_t};
extern "C" {
    fn XXH64_reset(state: *mut XXH64_State,
               seed: u64) -> c_int;

    fn XXH64_update(state: *mut XXH64_State,
                input: *const c_void,
                lenght: size_t) -> c_int;

    fn XXH64_digest(state: *const XXH64_State) -> u64;
}

impl XXH64_State {
    /// Create a new instance
    pub fn new() -> XXH64_State {
        XXH64_State {
            total_len: 0,
            v1:0,
            v2:0,
            v3:0,
            v4:0,
            mem64:[0;4],
            memsize:0,
            reserved:[0;2]
        }
    }

    /// Reset/Initialize XXH64_State with a given `seed`
    pub fn reset(&mut self, seed:u64) -> Result<(),()> {
        let result = unsafe {
            XXH64_reset(self, seed)
        };
        if result == 0 { Ok(()) } else { Err(()) }
    }

    /// Update hash state with the content of `input`
    pub fn update(&mut self, input: &[u8]) -> Result<(),()> {  
        let void_ptr = input.as_ptr() as *const c_void;
        let result = unsafe {
            XXH64_update(self,
                         void_ptr, 
                         input.len() as size_t)
        };
        if result == 0 { Ok(()) } else { Err(()) }
    }

    /// Produce a hash for any previously processed state
    /// from calls to `update()`
    pub fn digest(&self) -> u64 {
        unsafe {
            XXH64_digest(self)
        }
    }
}

// ------------------------------------------------------------------

#[test]
fn test_version_number() {
let expected = Version {
    major: 1,
    minor: 8,
    release: 3,
};
let result = get_version();
assert_eq!(result, expected);
}

#[test]
fn test_lz4_compression_hc() {
const DATA: &'static [u8] = &[
    0xfd, 0xb4, 0x50, 0x45, 0xcd, 0x3c, 0x15, 0x71, 0x3c, 0x87, 0xff, 0xe8, 0x5d, 0x20, 0xe7,
    0x5f, 0x38, 0x05, 0x4a, 0xc4, 0x58, 0x8f, 0xdc, 0x67, 0x1d, 0xb4, 0x64, 0xf2, 0xc5, 0x2c,
    0x15, 0xd8, 0x9a, 0xae, 0x23, 0x7d, 0xce, 0x4b, 0xeb,
];

let mut block =[0_u8;256];
assert!(can_compress(DATA.len(), block.len()));
let result = compress_hc(&mut block,&DATA); 
assert!(result.is_ok());

let mut decompressed = [0_u8;40];
let result_decompress = decompress(&mut decompressed, &block[0..result.unwrap_or(0)]);
assert!(result_decompress.is_ok());
let size_decomp = result_decompress.unwrap_or(0);
assert_eq!(size_decomp, DATA.len());

for i in 0..DATA.len() {
    assert_eq!(DATA[i], decompressed[i]);
}
}

#[test]
fn test_xxhash() {
    const DATA: &'static [u8] = &[
        0xfd, 0xb4, 0x50, 0x45, 0xcd, 0x3c, 0x15, 0x71, 0x3c, 0x87, 0xff, 0xe8, 0x5d, 0x20, 0xe7,
        0x5f, 0x38, 0x05, 0x4a, 0xc4, 0x58, 0x8f, 0xdc, 0x67, 0x1d, 0xb4, 0x64, 0xf2, 0xc5, 0x2c,
        0x15, 0xd8, 0x9a, 0xae, 0x23, 0x7d, 0xce, 0x4b, 0xeb,
    ];

    let mut state= XXH64_State::new();
    let mut result = state.reset(7);
    assert!(result.is_ok());
    result = state.update(&DATA);
    assert!(result.is_ok());
    let hash = state.digest();
    assert_eq!(hash, 8017375999291551053 as u64);
}
