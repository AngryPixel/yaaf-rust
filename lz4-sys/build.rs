/// Build C-dependencies
use std::env;
use std::fs;
use std::path::{Path, PathBuf};
use std::process::Command;
fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();
    let out_path = Path::new(&out_dir);
    let build_path = out_path.join("build-lz4");
    let install_path = out_path.join("install-lz4");
    let src_path_buf = PathBuf::from("lz4")
        .join("contrib")
        .join("cmake_unofficial");
    let src_path = fs::canonicalize(src_path_buf).expect("Failed to resolve src path");

    let install_lib_path = install_path.join("lib64");

    fs::create_dir_all(&build_path).expect("Failed to create build directory");

    // Configure LZ4
    let status_config = Command::new("cmake")
        .arg(&format!(
            "-DCMAKE_INSTALL_PREFIX:PATH={}",
            install_path.display()
        ))
        .arg(&"-DCMAKE_BUILD_TYPE=Release")
        .arg(&"-DBUILD_STATIC_LIBS=ON")
        .arg(src_path)
        .current_dir(&build_path)
        .status()
        .expect("Failed to configure LZ4");

    if !status_config.success() {
        panic!("Failed to configure LZ4");
    }

    // Build & Install lz4
    let status_install = Command::new("cmake")
        .args(&["--build", ".", "--target", "install", "--config", "Release"])
        .current_dir(&build_path)
        .status()
        .expect("Faild to build ZL4");

    if !status_install.success() {
        panic!("Failed to build/install LZ4");
    }

    println!(
        "cargo:rustc-link-search=native={}",
        install_lib_path.display()
    );
    println!("cargo:rustc-link-lib=static=lz4");
}
