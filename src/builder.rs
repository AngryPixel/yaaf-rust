/// Build Archives

use std::path::{Path, PathBuf};
use std::io;

struct Entry {
    full_path: PathBuf,
    archive_path: String,
    size_bytes: u64,
    time_last_mod: u64,
}

pub struct Builder {
    entries: Vec<Entry>, 
}


use std::io::Write;
/// Track the number of bytes written into Write
struct BytesWrittenTracker<'w,T:'w + Write> {
    writer: &'w mut T,
    bytes_written: u64
}

impl <'w, T:'w + Write> BytesWrittenTracker<'w, T> { 
    
    fn new(writer: &'w mut T) -> Self {
        BytesWrittenTracker{ writer, bytes_written: 0_u64 } 
    }   

    fn bytes_written(&self) -> u64 {
        return self.bytes_written;
    }
}

impl <'w, T:'w + Write> Write for BytesWrittenTracker<'w, T> {

    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let bytes_written = match self.writer.write(buf) {
            Ok(size) => size,
            Err(e) => return Err(e)
        };
        self.bytes_written += bytes_written as u64;
        return Ok(bytes_written);
    }

    fn flush(&mut self) -> io::Result<()> {
        self.writer.flush()
    }
}

/// Hash the bytes written to output
use lz4_sys::XXH64_State;
struct BytesHasher<'w, T:'w + Write> {
    writer: &'w mut T,
    hasher: XXH64_State
}

impl <'w, T:'w + Write> BytesHasher<'w,T> {
    
    fn new(writer:&'w mut T) -> std::result::Result<Self,()> {
        let mut hasher = XXH64_State::new();
        match hasher.reset(0) {
            Ok(_) => Ok(BytesHasher{writer, hasher}),
            Err(_) => Err(())
            
        }
    }

    fn digest(&self) -> u64 {
       self.hasher.digest()
    }
}

impl <'w, T:'w + Write> Write for BytesHasher<'w, T> {
    
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let bytes_written = match self.writer.write(buf) {
            Ok(size) => size,
            Err(e) => return Err(e)
        };
        if let Err(_) = self.hasher.update(&buf[0..bytes_written]) {
            return Err(io::Error::new(io::ErrorKind::Other,
                                    "Failed to hash written bytes"));
        }
        return Ok(bytes_written)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.writer.flush()
    }
}

use std::time::SystemTime;
fn system_time_to_archive_time(system_time:&SystemTime) -> u64 {
    if let Ok(duration) = system_time.duration_since(SystemTime::UNIX_EPOCH) {
        return duration.as_secs();
    }
    0
}
use entry;
use std::fs::OpenOptions;
use compression::*;
use byteorder::{WriteBytesExt, LE};
use manifest;
use std::collections::VecDeque;
use std::io::BufWriter;

impl Builder {
    
    pub fn new() -> Self {
        Builder { entries : Vec::new() }
    }

    fn add_file(&mut self, root_path: &Path, file_path: &Path) -> io::Result<()> {
        let metadata = file_path.metadata()?;
        
        // Check if file
        if !metadata.is_file() {
            return Err(io::Error::new(io::ErrorKind::InvalidInput
                                      , "Path does not point to a file"));
        }

        // Get relative path to root
        let path_relative = match file_path.strip_prefix(root_path) {
            Ok(path) => path,
            Err(_) => return Err(io::Error::new(io::ErrorKind::Other,
                                         "Path is not sub directory of root directory"))
        };

        assert!(path_relative.is_relative());

        let path_relative_as_string = match path_relative.to_str() {
            Some(x) => x,
            None => return Err(io::Error::new(io::ErrorKind::Other,
                                                 "Path is not valid utf8"))
        };
 
        // Get last time mod 
        let time_last_mod_sys = metadata.modified().unwrap_or(SystemTime::now());
        let time_last_mod = system_time_to_archive_time(&time_last_mod_sys); 
        // Get file size 
        let size_bytes = metadata.len();

        let entry = Entry {
            full_path: PathBuf::from(file_path),
            archive_path: path_relative_as_string.to_string(),
            size_bytes: size_bytes,
            time_last_mod: time_last_mod
        };
        
        self.entries.push(entry);
        Ok(())
    }

    pub fn scan_path(&mut self, path:&Path, recursive: bool) -> io::Result<u64> {
        
        if path.is_file() {
            self.add_file(path, path)?;
            return Ok(1u64);
        }

        if path.is_dir() && !recursive {
            return Ok(0);
        }
        
        let mut num_entries = 0;    
        let mut dir_queue = VecDeque::<PathBuf>::with_capacity(64);

        if path.is_dir() {
            for entry in std::fs::read_dir(path)? {
                let entry = entry?;
                dir_queue.push_back(entry.path());
            }
        }

        while let Some(cur_path) = dir_queue.pop_front() {
            if cur_path.is_file() {
                if let Err(e) = self.add_file(path, &cur_path) {
                    return Err(e);
                }
                num_entries += 1;
            } else if cur_path.is_dir() && recursive {
                for entry in std::fs::read_dir(&cur_path)? {
                    let entry = entry?;
                    let entry_path = entry.path();
                    if entry_path.is_dir() || entry_path.is_file() {
                        dir_queue.push_back(entry_path);
                    }
                }
            } 
        }

        Ok(num_entries)
    }

    pub fn build(&self, path:&Path) -> io::Result<(u64)> {
        
        let mut compressor =  match create_compressor(CompressorType::LZ4) {
            Some(c) => c,
            None => return Err(io::Error::new(io::ErrorKind::Other,
                                          "Failed to create compressor"))
        };

        let file = OpenOptions::new()
            .truncate(true)
            .create(true)
            .write(true)
            .open(path)?;
        let mut file = BufWriter::with_capacity(64 * 1024, file); 
        let mut bytes_tracker = BytesWrittenTracker::new(&mut file);
        let mut archive_entries:Vec<entry::Entry> = Vec::with_capacity(self.entries.len());
        let mut archive_offset:u64 = 0;
        for item in &self.entries {
            
            // skip over 0 size files
            if item.size_bytes == 0 {
                continue;
            }

            // Open original file 
            let mut entry_file = OpenOptions::new()
                .create_new(false)
                .read(true)
                .open(&item.full_path)?;

            // Compress file
            let (bytes_compressed,hash) = match compress(&mut bytes_tracker, 
                                                  &mut entry_file, 
                                                  &mut compressor) {
                Ok(size) => size,
                Err(e) => {
                    println!("Error: {}", e);
                    return Err(io::Error::new(io::ErrorKind::Other,
                                                    format!("Failed to compress file: {}", item.full_path.display())))
                }
            };

            // Create Entry header
            let mut entry = entry::Entry::new();
            entry.archive_offset = archive_offset;
            entry.time_last_mod = item.time_last_mod;
            entry.size_uncompressed = item.size_bytes;
            entry.name = item.archive_path.clone();
            entry.size_compressed = bytes_compressed as u64;
            entry.flags = compressor_type_flag(CompressorType::LZ4) as u16;
            entry.hash = hash;
            archive_entries.push(entry);

            archive_offset += bytes_compressed as u64;
        }

        let entries_hash;
        // record entries start offset
        let archive_offset_entry = bytes_tracker.bytes_written();
        {
            let mut byte_hasher = match BytesHasher::new(&mut bytes_tracker) {
                Ok(h) => h,
                Err(_) => return Err(io::Error::new(io::ErrorKind::Other,
                                                    "Failed to create byte hasher for entries"))
            };

            // write all entries to archive
            for entry in &archive_entries {
                entry::Entry::write(&mut byte_hasher, &entry)?;
            }

            entries_hash = byte_hasher.digest();
        }
        let archive_offset_manifest = bytes_tracker.bytes_written();
        
        // Create Manifest
        let mut manifest = manifest::Manifest::new();
        manifest.entries_count = archive_entries.len() as u64;
        manifest.entries_hash = entries_hash;
        manifest.entries_size = archive_offset_manifest 
            - archive_offset_entry;
        manifest.flags = 0;
        
        // Write manifest
        manifest::Manifest::write(&mut bytes_tracker, &manifest)?; 
        
        let manifest_size_bytes = bytes_tracker.bytes_written() 
            - archive_offset_manifest;

        // Write archive size
        bytes_tracker.write_u64::<LE>(manifest_size_bytes)?;
       
        // Flush buffered writer
        bytes_tracker.flush()?;


        Ok(bytes_tracker.bytes_written())
    }

}

#[test]
fn test_scan() {
    let mut builder = Builder::new();
    let result = builder.scan_path(Path::new("/home/lbeernaert/Projects"),true);
    let num_files = result.expect("Failed to scan path");
    assert_ne!(num_files, 0);
}
