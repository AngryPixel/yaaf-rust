use byteorder::{ReadBytesExt, WriteBytesExt, LE};
use std::io;
use std::io::{Read, Write};

pub struct Entry {
    magic: u32,
    pub size_compressed: u64,
    pub size_uncompressed: u64,
    pub hash: u64,
    pub time_last_mod: u64,
    pub archive_offset: u64,
    pub flags: u16,
    pub name: String,
}

impl Entry {
    
    const MAGIC:u32 = 0x137647f6;
    
    pub fn new() -> Entry {
        Entry {
            magic: Self::MAGIC,
            size_compressed: 0,
            size_uncompressed: 0,
            hash: 0,
            archive_offset: 0,
            time_last_mod: 0,
            flags: 0,
            name: String::new(),
        }
    }

    pub fn read(reader: &mut Read) -> io::Result<Entry> {
        let magic = reader.read_u32::<LE>()?;
        let size_compressed = reader.read_u64::<LE>()?;
        let size_uncompressed = reader.read_u64::<LE>()?;
        let hash = reader.read_u64::<LE>()?;
        let time_last_mod = reader.read_u64::<LE>()?;
        let archive_offset = reader.read_u64::<LE>()?;
        let flags = reader.read_u16::<LE>()?;
        let name_len = reader.read_u32::<LE>()? as usize;
        let mut name_buffer = Vec::with_capacity(name_len);
        name_buffer.resize(name_len, 0);
        reader.read_exact(&mut name_buffer)?;
        let name = match String::from_utf8(name_buffer) {
            Ok(n) => n,
            _ => return Err(io::Error::new(io::ErrorKind::Other, "unicode fail")),
        };

        Ok(Entry {
            magic,
            size_compressed,
            size_uncompressed,
            hash,
            archive_offset,
            time_last_mod,
            flags,
            name,
        })
    }

    pub fn write(writer: &mut Write, entry: &Entry) -> io::Result<()> {
        writer.write_u32::<LE>(entry.magic)?;
        writer.write_u64::<LE>(entry.size_compressed)?;
        writer.write_u64::<LE>(entry.size_uncompressed)?;
        writer.write_u64::<LE>(entry.hash)?;
        writer.write_u64::<LE>(entry.time_last_mod)?;
        writer.write_u64::<LE>(entry.archive_offset)?;
        writer.write_u16::<LE>(entry.flags)?;
        let name_bytes = entry.name.as_bytes();
        writer.write_u32::<LE>(name_bytes.len() as u32)?;
        writer.write_all(name_bytes)?;
        Ok(())
    }
}

#[cfg(test)]
use std::io::Cursor;
#[test]
fn test_entry_write_read() {
    let mut entry = Entry::new();
    entry.magic = 0xAABBCCDD;
    entry.size_compressed = 1024;
    entry.size_uncompressed = 2048;
    entry.hash = 0xDEADBEEF;
    entry.archive_offset = 66524;
    entry.time_last_mod = 12456;
    entry.flags = 255;
    entry.name = "EntryName".to_string();

    // write entry
    let mut cursor = Cursor::new(vec![0_u8; 128]);
    Entry::write(&mut cursor, &entry).expect("Failed to write entry");

    // read entry
    cursor.set_position(0);
    let entry_read = Entry::read(&mut cursor).expect("Failed to read entry");

    assert_eq!(entry.magic, entry_read.magic);
    assert_eq!(entry.size_compressed, entry_read.size_compressed);
    assert_eq!(entry.size_uncompressed, entry_read.size_uncompressed);
    assert_eq!(entry.hash, entry_read.hash);
    assert_eq!(entry.archive_offset, entry_read.archive_offset);
    assert_eq!(entry.time_last_mod, entry_read.time_last_mod);
    assert_eq!(entry.flags, entry_read.flags);
    assert_eq!(entry.name, entry_read.name);
}
