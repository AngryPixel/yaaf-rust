use byteorder::{ReadBytesExt, WriteBytesExt, LE};
use std::io;
use std::io::{Read, Write};

pub struct Manifest {
    magic: u32,
    version: u16,
    version_required: u16,
    pub entries_count: u64,
    pub entries_size: u64,
    pub entries_hash: u64,
    pub flags: u32,
}

impl Manifest {
    
    const MAGIC:u32 = 0x9fb18cbf;
    
    pub fn new() -> Manifest {
        Manifest {
            magic: Self::MAGIC,
            version: 0,
            version_required: 0,
            entries_count: 0,
            entries_size: 0,
            entries_hash: 0,
            flags: 0,
        }
    }

    pub fn read(reader: &mut Read) -> io::Result<Manifest> {
        let magic = reader.read_u32::<LE>()?;
        let version = reader.read_u16::<LE>()?;
        let version_required = reader.read_u16::<LE>()?;
        let entries_count = reader.read_u64::<LE>()?;
        let entries_size = reader.read_u64::<LE>()?;
        let entries_hash = reader.read_u64::<LE>()?;
        let flags = reader.read_u32::<LE>()?;

        Ok(Manifest {
            magic,
            version,
            version_required,
            entries_count,
            entries_size,
            entries_hash,
            flags,
        })
    }

    pub fn write(writer: &mut Write, manifest: &Manifest) -> io::Result<()> {
        writer.write_u32::<LE>(manifest.magic)?;
        writer.write_u16::<LE>(manifest.version)?;
        writer.write_u16::<LE>(manifest.version_required)?;
        writer.write_u64::<LE>(manifest.entries_count)?;
        writer.write_u64::<LE>(manifest.entries_size)?;
        writer.write_u64::<LE>(manifest.entries_hash)?;
        writer.write_u32::<LE>(manifest.flags)?;

        Ok(())
    }
}

#[cfg(test)]
use std::io::Cursor;
#[test]
fn test_manifest_write_read() {
    let mut manifest = Manifest::new();
    manifest.magic = 0xDEADBEEF;
    manifest.version = 128;
    manifest.version_required = 35600;
    manifest.entries_count = 256;
    manifest.entries_size = 200000000;
    manifest.entries_hash = 0xABCDEFCD;
    manifest.flags = 0xDEADBEEF;

    // write manifest
    let mut cursor = Cursor::new(vec![0_u8; 128]);
    Manifest::write(&mut cursor, &manifest).expect("Failed to write manifest");

    cursor.set_position(0);

    let manifest_read = Manifest::read(&mut cursor).expect("Failed to read manifest");

    assert_eq!(manifest_read.magic, manifest.magic);
    assert_eq!(manifest_read.version, manifest.version);
    assert_eq!(manifest_read.version_required, manifest.version_required);
    assert_eq!(manifest_read.entries_count, manifest.entries_count);
    assert_eq!(manifest_read.entries_size, manifest.entries_size);
    assert_eq!(manifest_read.entries_hash, manifest.entries_hash);
    assert_eq!(manifest_read.flags, manifest.flags);
}
