

pub struct FileHeader {

    magic : u32,
    hash: u32,
    size: u32,
}

impl FileHeader {
    const MAGIC:u32 = 0xa0116f80; 
}

