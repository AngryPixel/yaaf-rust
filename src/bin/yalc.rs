extern crate yaaf;

use std::env;
use yaaf::archive::*;

fn print_version() {
    println!("Copyright (C) 2018, Leander Beernaert\n");
    println!("yaaf {}.{}.{}", 0, 0, 0);
    println!();
}

fn print_help() {
    print_version();

    println!("Usage: yaafcl options [switches] archive arg0 ... argn");
    println!("options:");
    println!("  -c : Create archive with files/directories listed in args");
    println!();
    println!("switches:");
    println!("  -r : recursive");
    println!();
}

#[derive(Debug, PartialEq, Eq)]
enum Op {
    CreateArchive,
    ListArchive,
    Invalid
}

const FLAG_RECURSIVE:u32 = 1 << 0;

use yaaf::builder::Builder;
use std::path::{PathBuf,Path};
fn create_archive(archive_path_str:&String, 
                  args:&[String], 
                  _flags:u32) -> bool {
    
    if args.len() == 0 {
        println!("No paths supplied to create archive '{}'", archive_path_str);
        exit(-1);
    }

    let mut builder = Builder::new();

    for path_str in args {
        let path = Path::new(path_str.as_str());
        match builder.scan_path(&path, true) {
            Ok(count) => println!("{} files added from {} ", count, path_str),
            Err(e) => {
                println!("Failed to scan path {}: {}", path_str, e);
                return false 
            }
        };
    }
    let archive_path = Path::new(archive_path_str.as_str());
    match builder.build(&archive_path) {
        Ok(bytes_written) => {
            println!("Created Archive {}: {} bytes", archive_path_str,
                     bytes_written);
            true
        }
        Err(e) => {
            println!("Failed to create archive {}: {}", archive_path_str, e);
            false
        }
    }
}

fn list_archive(archive_path_str:&String,
                _args:&[String], 
                _flags:u32) -> bool {
    let path = PathBuf::from(archive_path_str);
    let file_handler = match FileHandlerMemoryMapped::new(&path) {
        Ok(h) => h,
        Err(e) => {
            println!("Failed to create memory mapped file handler for {}: {}", 
                     archive_path_str, e); 
            return false;
        }
    };
    let mut archive = Archive::new(Box::new(file_handler));
    if let Err(e) = archive.load() {
        println!("Failed to load {}: {}", archive_path_str, e);
        return false;
    }

    for entry in archive.entry_iter() {
        println!("Entry: {}", entry.name);
        println!("  size c  {}", entry.size_compressed);
        println!("  size u  {}", entry.size_uncompressed);
        println!("  hash    {}", entry.hash);
    }

    true
}

use std::process::exit;
fn main() { 
    let args:Vec<String>= env::args().collect();
        
    if args.len() < 2 {
        print_help();
        exit(-1);
    }

    let mut arg_idx:usize = 1;

    let op = match args[arg_idx].as_str() {
        "-c" => Op::CreateArchive,
        "-l" => Op::ListArchive,
        _ => Op::Invalid
    };

    if op == Op::Invalid {
        println!("Unkown option '{}'", args[arg_idx]);
        exit(-1);
    }

    arg_idx += 1;
    let mut flags = 0u32;
    while arg_idx < args.len() {
        
        match args[arg_idx].as_str() {
            "-r"=> flags |= FLAG_RECURSIVE,
            _ => break
        };

        arg_idx +=1;
    }

    if arg_idx >= args.len() {
        println!("Archive not specified");
        exit(-1);
    }

    let archive_path = &args[arg_idx];
    arg_idx+=1;

    let op_args = &args[arg_idx..];
    
    let result = match op {
        Op::CreateArchive => create_archive(archive_path,op_args, flags),
        Op::ListArchive => list_archive(archive_path, op_args, flags),
        Op::Invalid => {
            panic!("Should not have been reached!");
        }
    };
    
    if result == false {
        exit(-1);
    }
}
