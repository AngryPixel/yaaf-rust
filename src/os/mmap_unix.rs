// Unix Implementation of mmap

use libc;
use std::fs::File;
use std::os::unix::io::AsRawFd;
use std::{io, ptr};

#[derive(Debug)]
pub struct MMapImpl {
    ptr: *mut libc::c_void,
    size: usize,
}

impl MMapImpl {
    pub fn new(file: &File) -> io::Result<MMapImpl> {
        let metadata = file.metadata()?;
        let file_size = metadata.len();
        if file_size == 0 {
            return Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                "File must have non-zero size",
            ));
        }

        let fd = file.as_raw_fd();

        let mut result = ptr::null_mut();

        unsafe {
            let offset: libc::off_t = 0;
            let local = libc::mmap(
                result,
                file_size as usize,
                libc::PROT_READ,
                libc::MAP_SHARED,
                fd,
                offset,
            );
            if local == libc::MAP_FAILED {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidInput,
                    "Failed to map File",
                ));
            }
            result = local;
        }

        Ok(MMapImpl {
            ptr: result,
            size: file_size as usize,
        })
    }

    pub fn ptr(&self) -> *const u8 {
        self.ptr as *const u8
    }

    pub fn size(&self) -> usize {
        self.size
    }
}

impl Drop for MMapImpl {
    fn drop(&mut self) {
        assert_ne!(self.ptr, ptr::null_mut());
        unsafe {
            libc::munmap(self.ptr, self.size);
        }
    }
}

