// Memory Mapped File Implementation
// Yes, there's a crate for this, but DIY experience

#[cfg(unix)]
use os::mmap_unix::MMapImpl;

use std::fs::File;
use std::io;
use std::ops::Deref;
use std::slice;

#[derive(Debug)]
pub struct MMap {
    file: File,
    imp: MMapImpl,
}

impl MMap {
    pub fn new(file: File) -> io::Result<MMap> {
        match MMapImpl::new(&file) {
            Ok(imp) => Ok(MMap {
                file: file,
                imp: imp,
            }),
            Err(e) => Err(e),
        }
    }
}

impl Deref for MMap {
    type Target = [u8];

    #[inline]
    fn deref(&self) -> &[u8] {
        unsafe { slice::from_raw_parts(self.imp.ptr(), self.imp.size()) }
    }
}

impl AsRef<[u8]> for MMap {
    fn as_ref(&self) -> &[u8] {
        self.deref()
    }
}

#[test]
fn test_map() {
    let file = File::open("/home/lbeernaert/.vimrc").unwrap();
    let map = MMap::new(file).unwrap();

    assert_ne!(map.imp.size(), 0);
    assert!(!map.imp.ptr().is_null());

    let bytes = map.deref();
    println!("{:?}", bytes)
}
