/*
 * YAAF Archive layout
 * Each Manifest Entry is sorted alphabetically so that the
 * search can be done in log2(n) time. File entries are not required to
 * be sorted.
 *
 * [ YAAF_FileHeader 0      ]
 * [ YAAF File Data 0       ]
 *    [ Size of Block 0  ] 4 bytes
 *    [ Hash of Block 0  ] 4 bytes
 *    [ Data of Block 0  ]
 *       ...
 * [ End of File 0 Blocks   ] 8 bytes - all 0
 * [ YAAF_FileHeader N      ]
 * [ YAAF File Data N       ]
 *.....
 * [ YAAF Manifest Entry 0  ]
 * [ YAAF File Name 0       ]
 * [ YAAF File Extra 0      ]
 * ...
 * [ YAAF Manifest Entry N  ]
 * [ YAAF File Name N       ]
 * [ YAAF File Extra N      ]
 *
 * [ YAAF Manifest          ]
 * [ Manifest Size          ]
 */

use std::io;

use entry::Entry;
use manifest::Manifest;

/// Archive Reader, needs to have both read and seek capabilities
pub trait Reader : io::Read + io::Seek {
}

/// Trait for an archive file FileHandler . Whether this involves open the archive file
/// once, mapping it into memory or another strategy entirely, we just require
/// a reader to process/look up files in the archive
pub trait FileHandler {
    
    fn open(&mut self) -> io::Result<Box<Reader>>;
}


pub struct Archive {
    file: Box<FileHandler>,
    manifest: Manifest,
    entries: Vec<Entry>
}   


use byteorder::{ReadBytesExt,LE};
impl Archive {

    pub fn new(file: Box<FileHandler>) -> Archive {
        Archive { file, 
            manifest: Manifest::new(), 
            entries: Vec::new() 
        }
    }

    pub fn is_loaded(&self) -> bool {
        return !self.entries.is_empty();
    }

    pub fn load(&mut self) -> io::Result<()> {
        if self.is_loaded() {
            return Err(io::Error::new(io::ErrorKind::Other,
                                      "Archive already loaded"));
        }

        let mut reader = self.file.open()?;
       
        // seek end
        let size_of_u64 = std::mem::size_of::<u64>() as u64;
        let seek_offset:i64 = -(size_of_u64 as i64);
        reader.seek(io::SeekFrom::End(seek_offset))?;
            
        let manifest_size = reader.read_u64::<LE>()?;
        let manifest_offset = - ((manifest_size + size_of_u64) as i64);

        // Seek manifest start
        reader.seek(io::SeekFrom::End(manifest_offset))?;

        // Read Manifest 
        let manifest = Manifest::read(&mut reader)?;
        
        // TODO: Verify version numbers, check hash
        
        // Go to entries start
        let entries_offset = manifest_offset - (manifest.entries_size as i64);
        reader.seek(io::SeekFrom::End(entries_offset))?;

        // Reserve capacity
        let mut entries:Vec<Entry> = 
            Vec::with_capacity(manifest.entries_count as usize);

        for _ in 0..manifest.entries_count {
            let entry = Entry::read(&mut reader)?;
            entries.push(entry);
        }

        self.entries = entries;
        self.manifest = manifest;
        Ok(())
    }

    pub fn entry_iter(&self) -> impl Iterator<Item = &Entry> {
        self.entries.iter()
    }
}


use std::path::{PathBuf,Path};
pub struct FileHandlerDefault {
    path: PathBuf
}

impl FileHandlerDefault {
    pub fn new(path:&Path) -> Self {
        Self {path: PathBuf::from(path) }
    }
}

use std::fs::OpenOptions;
impl FileHandler for FileHandlerDefault {
    
    fn open(&mut self) -> io::Result<Box<Reader>> {
        let file = OpenOptions::new()
                .read(true)
                .create_new(false)
                .open(&self.path)?;
       Ok(Box::new(file))
    }
}

impl Reader for std::fs::File {}

use os::mmap::MMap;
use std::sync::Arc;
pub struct FileHandlerMemoryMapped {
    mmap: Arc<MMap>
}

impl FileHandlerMemoryMapped {
    pub fn new(path: &Path) -> io::Result<Self> {
        let file = OpenOptions::new()
            .read(true)
            .create_new(false)
            .open(path)?;
        let mmap = MMap::new(file)?;
        Ok(Self {mmap: Arc::new(mmap)})
    }
}

/// Wrapper struct so we can implement AsRef over Arc<>
struct ReaderMemoryMappedWrapper {
    mmap: Arc<MMap>
}

impl AsRef<[u8]> for ReaderMemoryMappedWrapper { 
    fn as_ref(&self) -> &[u8] {
        return self.mmap.as_ref();
    }
}

use std::io::Cursor;

impl <T> Reader for std::io::Cursor<T>
    where T: AsRef<[u8]> {}

impl FileHandler for FileHandlerMemoryMapped {
    fn open(&mut self) -> io::Result<Box<Reader>> {
        let reader = ReaderMemoryMappedWrapper { mmap: self.mmap.clone() };
        let cursor = Cursor::new(reader);
        Ok(Box::new(cursor))
    }
}

