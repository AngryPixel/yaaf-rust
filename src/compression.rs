
/// Compression result. When successfull contains the number of compressed bytes
pub type Result = std::result::Result<usize,()>; 

/// Required behaviour for any compression system
pub trait Compressor {

    /// Compress input into output
    fn compress(&mut self, output: &mut[u8], input: &[u8]) -> Result;

    /// Decompress input into output
    fn decompress(&mut self, output: &mut[u8], input: &[u8]) -> Result;

    /// Get the preffered block size for any input for this compressor
    fn preferred_input_block_size(&self) -> u32;

    /// Get the preferred block size for any output for this compressor 
    fn preferred_compression_block_size(&self) -> u32;
}

/// Available comression types
pub enum CompressorType {
    LZ4
}

/// Get the flag value for a compressor type
pub fn compressor_type_flag(compressor_type: CompressorType) -> u8 {
    match compressor_type {
        CompressorType::LZ4 => 0x01,
    }
}

/// Create a new instance of a compressor based on `compressor_type`
pub fn create_compressor(compressor_type:CompressorType) -> Option<impl Compressor> {
    match compressor_type {
        CompressorType::LZ4 => Some(CompressorLz4{}),
    }
}


use std::io::{Read,Write};
use std::io;
use lz4_sys::XXH64_State;

/// Compress from input into output
/// \return (Number of bytes compressed, hash of input)
pub fn compress(output: &mut Write, 
                input: &mut Read, 
                compressor: &mut Compressor) -> io::Result<(usize, u64)>{

    let mut buffer_read = vec![0_u8;compressor.preferred_input_block_size() as usize];
    let mut buffer_write = vec![0_u8;compressor.preferred_compression_block_size() as usize];
    let mut hasher = XXH64_State::new();
    let mut bytes_written_total = 0_usize; 

    if let Err(_) = hasher.reset(0) {
        return Err(io::Error::new(io::ErrorKind::Other,
                                  "Failed to init hashing context"));
    }

    loop {
        let bytes_read = input.read(buffer_read.as_mut_slice())?;
        if bytes_read == 0 {
            break;
        }

        // Create view of read data only
        let input_slice = &buffer_read[0..bytes_read];

        // Update hash of file
        if hasher.update(input_slice).is_err() {
            return Err(io::Error::new(io::ErrorKind::Other,
                                      "Failed to hash input"));
        }

        // Compress read data
        let bytes_compressed = match compressor.compress(buffer_write.as_mut_slice(), input_slice) {
            Ok(size) => size,
            Err(_) => return Err(io::Error::new(io::ErrorKind::Other,
                                                "Failed to compress read input"))
        };

        // Create view of compressed data
        let output_slice = &buffer_write[0..bytes_compressed];

        // Write Compressed data
        output.write_all(output_slice)?;

        bytes_written_total += bytes_compressed;
    }

    if bytes_written_total == 0 {
        return Err(io::Error::new(io::ErrorKind::InvalidData,
                                  "Did not write any output"));
    }

    Ok((bytes_written_total, hasher.digest()))
}


// LZ4 Compression Implementation

struct CompressorLz4 {}

impl CompressorLz4 {
    const BLOCK_SIZE_INPUT:u32 = 128 * 1024;
    const BLOCK_SIZE_WRITE:u32 = CompressorLz4::BLOCK_SIZE_INPUT + (8 * 1024);
}

impl Compressor for CompressorLz4 {

    fn compress(&mut self, output: &mut[u8], input: &[u8]) -> Result {
        match lz4_sys::compress_hc(output, input) {
            Ok(size) => Ok(size),
            _ => Err(())
        }
    }

    fn decompress(&mut self, output: &mut[u8], input: &[u8]) -> Result {
        match lz4_sys::decompress(output, input) {
            Ok(size) => Ok(size),
            _ => Err(())
        }
    }

    fn preferred_input_block_size(&self) -> u32 {
        CompressorLz4::BLOCK_SIZE_INPUT
    }

    fn preferred_compression_block_size(&self) -> u32 {
        CompressorLz4::BLOCK_SIZE_WRITE
    }
}



